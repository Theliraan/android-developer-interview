package mera.com.testapp.api.db;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import mera.com.testapp.api.models.State;

public class StateTable {
    public static final String TABLE_STATE = "state_table";
    public static final String KEY_STATE_ICAO = "state_icao";
    public static final String KEY_STATE_CALLSIGN = "state_callsign";
    public static final String KEY_STATE_COUNTRY = "state_country";
    public static final String KEY_STATE_VELOCITY = "state_velocity";

    public static final String CREATE_TABLE_STATE = "CREATE TABLE IF NOT EXISTS " + TABLE_STATE +
            " (" +
            KEY_STATE_ICAO + " TEXT, " +
            KEY_STATE_CALLSIGN + " TEXT, " +
            KEY_STATE_COUNTRY + " TEXT, " +
            KEY_STATE_VELOCITY + " TEXT " +
            ")";

    @NonNull
    public static State convert(Cursor cursor) {
        String icao = cursor.getString(cursor.getColumnIndex("state_icao"));
        String callsign = cursor.getString(cursor.getColumnIndex("state_callsign"));
        String country = cursor.getString(cursor.getColumnIndex("state_country"));
        String velocity = cursor.getString(cursor.getColumnIndex("state_velocity"));
        return new State(icao, callsign, country, TextUtils.isEmpty(velocity) ? 0f : Float.parseFloat(velocity));
    }
}