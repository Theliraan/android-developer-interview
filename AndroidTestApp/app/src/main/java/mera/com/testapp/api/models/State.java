package mera.com.testapp.api.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;

public class State implements Parcelable {
    private String mIcao24;
    private String mCallsign;
    private String mOriginCountry;
    private float mTimePosition;
    private float mTimeVelocity;
    private float mLongitude;
    private float mLatitude;
    private float mAltitude;
    private boolean mIsOnGround;
    private float mVelocity;
    private float mHeading;
    private float mVerticalRate;

    public State(String icao, String callsign, String country, float velocity) {
        this.mIcao24 = icao;
        this.mCallsign = callsign;
        this.mOriginCountry = country;
        this.mVelocity = velocity;
    }

    private State(Parcel parcel) {
        mIcao24 = parcel.readString();
        mCallsign = parcel.readString();
        mOriginCountry = parcel.readString();
        mTimePosition = parcel.readFloat();
        mTimeVelocity = parcel.readFloat();
        mLongitude = parcel.readFloat();
        mLatitude = parcel.readFloat();
        mAltitude = parcel.readFloat();
        mIsOnGround = parcel.readByte() != 0;
        mVelocity = parcel.readFloat();
        mHeading = parcel.readFloat();
        mVerticalRate = parcel.readFloat();
    }

    public String getIcao24() {
        return mIcao24;
    }

    public String getCallsign() {
        return mCallsign;
    }

    public String getOriginCountry() {
        return mOriginCountry;
    }

    public float getVelocity() {
        return mVelocity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mIcao24);
        dest.writeString(mCallsign);
        dest.writeString(mOriginCountry);
        dest.writeFloat(mTimePosition);
        dest.writeFloat(mTimeVelocity);
        dest.writeFloat(mLongitude);
        dest.writeFloat(mLatitude);
        dest.writeFloat(mAltitude);
        dest.writeByte((byte) (mIsOnGround ? 1 : 0));
        dest.writeFloat(mVelocity);
        dest.writeFloat(mHeading);
        dest.writeFloat(mVerticalRate);
    }

    public static final Parcelable.Creator<State> CREATOR = new Parcelable.Creator<State>() {
        @NonNull
        public State createFromParcel(Parcel in) {
            return new State(in);
        }

        @NonNull
        public State[] newArray(int size) {
            return new State[size];
        }
    };

    public static State parse(ArrayList<String> stateRaw) {
        return new State(
                stateRaw.get(0),
                stateRaw.get(1),
                stateRaw.get(2),
                Float.parseFloat(stateRaw.get(10))
        );
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof State)) {
            return false;
        }

        State state = (State) obj;
        return TextUtils.equals(mIcao24, state.mIcao24)
                && TextUtils.equals(mCallsign, state.mCallsign)
                && TextUtils.equals(mOriginCountry, state.mOriginCountry)
                && mVelocity == state.mVelocity;
    }

    @Override
    public int hashCode() {
        return ((Object) mLongitude).hashCode() +
                ((Object) mLatitude).hashCode() +
                ((Object) mAltitude).hashCode();
    }
}