package mera.com.testapp.ui;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Set;

import mera.com.testapp.R;
import mera.com.testapp.api.models.State;

class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private State[] mDataset;

    static class ViewHolder extends RecyclerView.ViewHolder {
        View rootView;
        TextView icao24;
        TextView callsign;
        TextView origin_country;
        TextView velocity;
        ViewHolder(View view) {
            super(view);
            rootView = view;
        }
    }

    ListAdapter() {
        mDataset = new State[0];
    }

    public void setData(Set<State> dataset) {
        mDataset = dataset.toArray(new State[dataset.size()]);
        notifyDataSetChanged();
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.icao24 = (TextView) view.findViewById(R.id.icao24);
        viewHolder.callsign = (TextView) view.findViewById(R.id.callsign);
        viewHolder.origin_country = (TextView) view.findViewById(R.id.origin_country);
        viewHolder.velocity = (TextView) view.findViewById(R.id.velocity);

        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.icao24.setText(mDataset[position].getIcao24());
        holder.callsign.setText(mDataset[position].getCallsign());
        holder.origin_country.setText(mDataset[position].getOriginCountry());
        holder.velocity.setText(Float.toString(mDataset[position].getVelocity()) + " m / s");
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
