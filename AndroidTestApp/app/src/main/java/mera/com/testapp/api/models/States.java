package mera.com.testapp.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class States {
    @SerializedName("time")
    private long mTime;

    @SerializedName("states")
    private ArrayList<ArrayList<String>> mStates;

    public long getTime() {
        return mTime;
    }

    public ArrayList<ArrayList<String>> getStates() {
        return mStates;
    }
}